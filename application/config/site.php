<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*	Site Settings
*	
*	@author 	Noerman Agustiyan (@anoerman)
*	@version 	0.1.1
*	
*	Descriptions : 	You can add, edit or delete this site setting based on your 
*					preferences. Make sure the config name is unique.
*	
*/

$config['site_name']    = "EMC";
$config['site_slogan']  = "E-ticketing Monitoring Centre";
$config['site_author']  = "Commuter Line";
$config['site_company'] = "PT. Kereta Commuter Indonesia";

